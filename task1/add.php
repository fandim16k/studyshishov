<?php 
/*
Two tasks: 
1) find min , max , avg in random array 
2) reverse string 
Author: Mikhail Shishov
*/

//Task1 :Get min , max , avg in array 
$rand_array=array();
//fill array by random numbers
for ($i=0;$i<=10;$i++){
    $rand_array[$i]=rand(-11,25);
}

var_dump($rand_array);
echo "<br>";
//get minimum value
function find_min_in_array(array $array){
    $min=1; 
    for($i=0;$i<count($array);$i++){
        if($array[$i]<$min){
            $min = $array[$i];
        }
    }
    echo "Min:"." ".$min;
}
//get max value
function find_max_in_array(array $array){    
    $max=1;
    for($i=0;$i<=count($array);$i++){
        if($array[$i]>=$max){
            $max = $array[$i];
        }
    }
    echo "Max:"." ".$max;    
}
//get average value in array 
function find_avg_in_array(array $array){
  $counter=0; // counter to count all elements in the array
  $amount_elements=count($array);
  for($i=0;$i<=count($array);$i++){
      $counter +=$array[$i];
  }
  $result=$amount_elements/$counter;
  echo "Average value in array:".$result;   
}

find_min_in_array($rand_array);
find_max_in_array($rand_array);
find_avg_in_array($rand_array);



?>

<form method="post" action=" ">
    <input type="text" name="string" placeholder="Введите строку ">
    <input type="submit"> 
</form> 

<?php
//Task2 :Reverse string 
$string_var = $_POST['string'];
//get reverse string 
$str=strrev($string_var);
if($str != " "){
    echo "Reverse string:".$str;
}

?>